# WineCleanAndAnalisis
## Calidad del vino basado en pruebas fisicoquímicas
## Autor
David Bravo

## Acerca de este software
 - Este software es parte de la Práctica 2 de la asignatura: "Tipologia y ciclo de vida de los datos".
- Asignatura: Tipologia y ciclo de vida de los datos.
- Master de Data Science.
- Universitat Oberta of Catalunya.

## Dataset
Puede chequear el dataset en la siguiente dirección:

https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/

## Licencia
El contenido de este proyecto esta licencia bajo la [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/), y el código fuente usadao para mostrar este contenido esta licenciado bajo la [MIT license](https://opensource.org/licenses/mit-license.php).
