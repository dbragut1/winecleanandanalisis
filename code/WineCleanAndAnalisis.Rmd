---
title: "Tipologia y ciclo de vida de los datos - Práctica 2"
subtitle: "Universitat Oberta of Catalunya - Master de Data Science"
author: "David Bravo"
date: "12/31/2021"
output:
  html_document:
    toc: true
    toc_depth: 4
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


## 1. Descripción del dataset
El conjunto de datos objeto de análisis se ha creado por: Paulo Cortez (Univ. Minho), Antonio Cerdeira, Fernando Almeida, Telmo Matos y Jose Reis (CVRVV) @ 2009. 

Obtenido a partir de este enlace de [UCI](https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/) y está conformado por dos conjuntos de datos, separados por tipología de color del vino, utilizando muestras de vino tinto y blanco. Ambos están compuestos por doce características (columnas) con un número de instancias(filas): vino tinto - 1599; vino blanco - 4898. Entre los campos de este conjunto de datos, encontramos los siguientes:

**Variables de entrada (basadas en pruebas fisicoquímicas):**

- **fixed acidity:** La acidez Acidez fija , es una medida de la concentración total de ácidos titulables e iones de hidrógeno libres presentes en su vino. Se puede usar un papel tornasol para identificar si una solución dada es ácida o básica. Los ácidos titulables más comunes son el ácido tartárico, málico, cítrico y carbónico. Estos ácidos, junto con muchos más en cantidades más pequeñas, se encuentran naturalmente en las uvas o se crean a través del proceso de fermentación.
- **volatile acidity:** La acidez volátil es causada principalmente por bacterias en el vino que crean ácido acético, el ácido que le da al vinagre su sabor y aroma característicos, y su subproducto, el acetato de etilo. La acidez volátil podría ser un indicador de deterioro o errores en los procesos de fabricación, causados por cosas como uvas dañadas, vino expuesto al aire, etc. Esto hace que las bacterias del ácido acético entren y prosperen, dando lugar a sabores y olores desagradables. ¡Los expertos en vino a menudo pueden decir esto con solo olerlo!
- **citric acid:** El ácido cítrico se encuentra generalmente en cantidades muy pequeñas en las uvas de vinificación. Actúa como conservante y se agrega a los vinos para aumentar la acidez, complementar un sabor específico o prevenir las brumas férricas. Se puede agregar a los vinos terminados para aumentar la acidez y dar un sabor "fresco". Sin embargo, el exceso de adición puede arruinar el sabor.
- **residual sugar:** Azúcar residual, o RS para abreviar, se refiere a los azúcares naturales de uva que quedan después de que cesa la fermentación (ya sea a propósito o no). El jugo de las uvas de vino comienza intensamente dulce, y la fermentación consume ese azúcar cuando las levaduras se deleitan con él. Durante la elaboración del vino, la levadura normalmente convierte todo el azúcar en alcohol y produce un vino seco. Sin embargo, a veces no todo el azúcar es fermentado por la levadura, dejando algo de dulzor sobrante. Para hacer un vino que sepa bien, la clave es tener un equilibrio perfecto entre el dulzor y la acidez de la bebida.
- **chlorides:** La cantidad de cloruros presentes en un vino suele ser un indicador de su "salinidad". Esto suele estar influenciado por el territorio donde crecieron las uvas de vinificación, los métodos de cultivo y también el tipo de uva. Demasiada salinidad se considera indeseable. La proporción adecuada puede hacer que el vino sea más sabroso.
- **free sulfur dioxide:** El dióxido de azufre libre es la forma libre de SO2 (en mg / dm ^ 3) que existe en equilibrio entre el SO2 molecular (como gas disuelto) y el ion bisulfito; Previene el crecimiento microbiano y la oxidación del vino.
- **total sulfur dioxide:** El dióxido de azufre total es la suma de la cantidad de formas libres y unidas de SO2; en concentraciones bajas, el SO2 es mayormente indetectable en el vino, pero en concentraciones de SO2 libre superiores a 50 ppm, el SO2 se hace evidente en la nariz y el sabor del vino.
- **density:** También conocido como gravedad específica, se puede utilizar para medir la concentración de alcohol en los vinos. Durante la fermentación, el azúcar del jugo se convierte en etanol con dióxido de carbono como gas residual. El seguimiento de la densidad durante el proceso permite un control óptimo de este paso de conversión para vinos de la más alta calidad. Los vinos más dulces generalmente tienen densidades más altas.
- **pH:** El pH representa el poder del hidrógeno, que es una medida de la concentración de iones de hidrógeno en la solución. Generalmente, las soluciones con un valor de pH inferior a 7 se consideran ácidas, y algunos de los ácidos más fuertes se acercan a 0. Las soluciones por encima de 7 se consideran alcalinas o básicas. El valor de pH del agua es 7, ya que no es ni un ácido ni una base.
- **sulphates:** Los sulfatos son sales de ácido sulfúrico. No participan en la producción de vino, pero algunos fabricantes de cerveza utilizan sulfato de calcio, también conocido como yeso de los cerveceros, para corregir las deficiencias minerales en el agua durante el proceso de elaboración. También agrega un poco de sabor "fuerte".
- **alcohol:** El alcohol se forma como resultado de la levadura que convierte el azúcar durante el proceso de fermentación.

**Variable de salida (basada en datos sensoriales):**

- quality: Calidad (puntuación entre 0 y 10)

### Importancia y objetivos de los análisis
A partir de este conjunto de datos se plantea la problemática de determinar qué variables influyen más sobre la calidad del vino. Además, se podrá proceder a crear modelos de regresión que permitan predecir la calidad de un vino en función de sus características y contrastes de hipótesis que ayuden a identificar propiedades interesantes. 

# 2. Integración y selección de los datos de interés a analizar.
Antes de comenzar con la limpieza de los datos, procedemos a realizar la lectura de los ficheros (vino tinto y blanco) en formato CSV en el que se encuentran. El resultado devuelto por la llamada a la función read.csv() será un objeto data.frame. del que generaremos una combinación.

```{r CargaLibrerias}
# Carga de las librerías
suppressMessages(library(skimr)) # Skimr está diseñado para proporcionar estadísticas resumidas sobre variables en marcos de datos, tibbles, tablas de datos y vectores.
suppressMessages(library(ggstatsplot)) # Ggstatsplot es una extensión del paquete {ggplot2} para crear gráficos con detalles de pruebas estadísticas.
suppressMessages(library(nortest)) # Nortest utilizado para realizar el test de Anderson para realizar el test de distribución.
suppressMessages(library(ggplot2)) # Ggplot2 visualización de los datos.
suppressMessages(library(tidyr)) # Tidyr cambia el diseño de los conjuntos de datos.
suppressMessages(library(gridExtra)) # GridExtra para construir gráficos.
suppressMessages(library(corrplot)) # Corrplot para gráfico de coeficientes de correlación.
```

```{r CargaFicheros}
# Carga de los ficheros directamente de la fuente de información. También disponible en el repositorio del proyecto

df_wineRed <- read.csv(file = "https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/winequality-red.csv", sep = ";",header=TRUE)
colour <- rep("red", dim(df_wineRed)[1])
df_wineRed$wine.colour <- colour
df_wineWhite <- read.csv(file = "https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/winequality-white.csv", sep = ";",header=TRUE)
colour <- rep("white", dim(df_wineWhite)[1])
df_wineWhite$wine.colour <- colour
```


```{r ValidacionDimensiones}
# Dimensiones
dim(df_wineRed)
dim(df_wineWhite)
```
```{r CreacionFicheroWine}
# Para crear un fichero que tenga balanceada las muestras de ambos vinos creamos un subconjunto
df_wineWhite_sample<-df_wineWhite[0:1599,]
dim(df_wineWhite_sample)

# Creación de un fichero con las muestras balanceadas
df_wine<-rbind(df_wineRed,df_wineWhite_sample)
df_wine$wine.colour <- factor(df_wine$wine.colour)

dim(df_wine)
names(df_wine)
summary(df_wine)
```

El fichero generado de datos contiene 3198 y 13 variables.
Contiene 1599 registros de vino tinto y 1599 de vino blanco. Estando balanceada la cantidad de registros de vino blanco y tinto.
Las variables son "fixed.acidity", "volatile.acidity","citric.acid","residual.sugar","chlorides","free.sulfur.dioxide","total.sulfur.dioxide","density", "pH","sulphates","alcohol","quality" y "colour". 

- **total.sulfur.dioxide**: Tiene una gran dispersion desde 6 hasta 366,5.
- **alcohol**: Varía desde 8,4 hasta 14.9%.
- **residual.sugar**: Tiene una gran dispersion desde 0.8 hasta 22.
- **density/pH/sulphates**: Tienen poca dispersión.
- **quality**: Varía desde 3 hasta 9.

# 3. Limpieza de datos
## 3.1 ¿Los datos contienen ceros o elementos vacíos?¿Cómo gestionarías cada uno de estos casos?
Los datos no contienen valores vacios. Los valores a 0, se encuentran en la variable:

- **citric.acid**: Es un valor normal como podemos observar en el siguiente sumario son valores normales.


Contestando a la pregunta de "**¿Cómo gestionarias cada uno de los casos?**", va a depender de cada tipo de variable, su distribución y el sentido que tenga. Llegando a tener que eliminar por completo el registro o actualizando el valor.
```{r tipoDato}
skim(df_wine)
```

# 3.2. Identificación y tratamiento de valores extremos.

Los valores extremos o outliers son aquellos que parecen no ser congruentes sin los comparamos con el resto de los datos. 
Vamos a realizar una primera previsualización de los valores extremos utilizando boxplot. 

```{r IdentificacionExtremos}
par(mfrow=c(2,2))
for(i in 1:ncol(df_wine)){
  boxplot(df_wine[,i],main=colnames(df_wine)[i],width=50)
}
```

Mediante los gráficos de boxplots intuimos la presencia de outliers pero no es la manera más efectiva de identificarlos, ya que a primera vista podemos deducir un outlier al estar muy alejado del resto de puntos. 
Para realizar su eliminación se pueden utilizar:

**1) Rango intercuantil**: En un conjunto de datos, es la diferencia entre el percentil 75 (Q3) y el percentil 25 (Q1). El rango intercuartil (IQR) es una medida de la dispersión de valores en el medio 50%.
Si una observación es 1,5 veces el rango intercuartil más que el tercer cuartil (Q3) o 1,5 veces el rango intercuartílico menor que el primer cuartil (Q1), se considera un valor atípico (Q1).

**2) Z-score**: El Z-score que es una medida numérica que describe la relación de un valor con la media de un grupo de valores.Se mide en términos de desviación estándar de la media. 
A z-score es calculado usando la siguiente formula: z = (X – μ) / σ en donde:

- X es el valor del dato.
- μ es la media de la población.
- σ es la desviación estandar de la población.

Si Z-score de una observación es menor que -3 o mayor que 3, se considera un valor atípico. 

Vamos a utilizar la segunda técnica Rango Intercuantil. Para el conjunto de datos de 3198 registros se han identificado 160 registros que serían outliers.

```{r TratamientoExtremos}
is_outlier <- function(x) {
  qs = quantile(x, probs = c(0.25, 0.75), na.rm = FALSE)
  qs1 <- qs[1]
  qs2 <- qs[2]
  iqr = qs2 - qs1 
  
  threshold_lower = qs1 - (iqr * 3)
  threshold_upper = (iqr * 3) + qs2
  # Devuelve vector outliers
  x > threshold_upper | x < threshold_lower
}

remove_outliers <- function(df, cols = names(df)) {
  for (col in cols) {
    cat("Elimina outliers en la columna: ", col, " \n")
    df <- df[!is_outlier(df[[col]]),]
  }
  df
}

df_wine_no_outliers <- remove_outliers(df_wine, names(df_wine[1:11]))
write.csv(df_wine_no_outliers,"./df_wine_no_outliers.csv", row.names = TRUE)

dim(df_wine_no_outliers)
```

# 4. Análisis de los datos.
## 4.1. Selección de los grupos de datos que se quieren analizar/comparar (planificación de los análisis a aplicar).
A continuación, seleccionamos el conjunto de datos que son interesantes para analizar/comparar:
```{r Agrupacion}
#Agrupación por color
df_wine.red <- df_wine_no_outliers[df_wine_no_outliers$wine.colour == "red",]
df_wine.white <- df_wine_no_outliers[df_wine_no_outliers$wine.colour == "white",]
```



## 4.2. Comprobación de la normalidad y homogeneidad de la varianza.

### 4.2.1 Comprobación de la normalidad

Vamos a realizar una revision de la normalidad de cada una de las variables, para ello hemos creado una función para ayudarnos a crear un histograma y ver la distribución de los valores. 
Además, utilizaremos la prueba de normalidad de Anderson-Darling para la comprobar que nuestras variables cuantitativas provienen de una distribuida normal 
Si la variable obtiene un valor superior al nivel prefijado α = 0, 05, entonces se considera una distribución normal.



```{r NormalidadAnderson}
isNormalAnderson <- function(var_name, data_val=df_wine_no_outliers, val_alpha = 0.05) {
  ad_val = ad.test(data_val[,var_name])$p.value
  print(ad_val)
  if (ad_val < val_alpha) {
    return(cat(var_name," según el test de Anderson NO es una distribución normal"))
  }else{
    return(cat(var_name," según el test de Anderson SÍ es una distribución normal"))
  }
}
``` 

```{r Normalidad}
create_plot <- function(var_name, data_val=df_wine_no_outliers, binwidth = 0.1) {
  return(ggplot(aes_string(x = var_name), data = data_val) + geom_histogram(binwidth = binwidth))
}
```

```{r Transfomation.Output}
df_wine_trans<-df_wine_no_outliers
```

#### 4.2.1.1 fixed.acidity

Tiene una asimetría positiva. 

```{r Normalidad fixed.acidity}
isNormalAnderson('fixed.acidity')
create_plot('fixed.acidity')
```

El intento de transformar(log10) los datos fixed.acidity funcionó, ajustándolo a la normal.

```{r Normalidad fixed.acidity.transform}
create_plot('fixed.acidity') + scale_x_log10()
df_wine_trans$fixed.acidity<-log10(df_wine_trans$fixed.acidity)
```

#### 4.2.1.2 volatile.acidity

Tiene una asimetría positiva.

```{r Normalidad volatile.acidity}
isNormalAnderson('volatile.acidity')
create_plot('volatile.acidity')
```

El intento de transformar los datos (log10) funcionó, ajustandolo a la normal.
```{r Normalidad volatile.acidity.transform}
create_plot('volatile.acidity') + scale_x_log10()
df_wine_trans$volatile.acidity<-log10(df_wine_trans$volatile.acidity)
```


#### 4.2.1.3 citric.acid

No se ajusta a la distribución normal y es asimetrica. 


```{r Normalidad citric.acid}
isNormalAnderson('citric.acid')
create_plot('citric.acid')
```

El intento de transformar los datos (log10) no funcionó y genera una asimetría negativa.

```{r Normalidad citric.acid.transform, echo=FALSE, message=FALSE, warning=FALSE}

create_plot('citric.acid') + scale_x_log10()
```


#### 4.2.1.4 residual.sugar

Tiene una distribución asimétrica positiva. 

```{r Normalidad residual.sugar}
isNormalAnderson('residual.sugar')
create_plot('residual.sugar')
```

**residual.sugar** aun realizando la transforamda, tiene una distribución sesgada hacia la derecha. 

```{r Normalidad citric.acid transformada}
create_plot('residual.sugar')+ scale_x_log10()
```

Vemos que **residual.sugar** sigue teniendo outlier 16.2 y genera esa distribución.

```{r Normalidad residual.sugar transformada summary}
summary(df_wine_no_outliers$residual.sugar)
```
#### 4.2.1.5 chlorides

Tiene una distribución asimétrica. 

```{r Normalidad chlorides}
isNormalAnderson('chlorides')
create_plot('chlorides')
```

Aplicando la transformación (log10) ha normalizado la distribución.
```{r Normalidad chlorides.transform}
create_plot('chlorides') +scale_x_log10()
df_wine_trans$chlorides<-log10(df_wine_trans$chlorides)

```

#### 4.2.1.6 free.sulfur.dioxide

La distribución está sesgada hacia la derecha.

```{r Normalidad free.sulfur.dioxide}
isNormalAnderson('free.sulfur.dioxide')
create_plot('free.sulfur.dioxide')
```

Aplicando la transformación (log10) resulta una distribución asimétrica negativa.


```{r Normalidad free.sulfur.dioxide.transform}
create_plot('free.sulfur.dioxide')+scale_x_log10()
```

#### 4.2.1.7 total.sulfur.dioxide

La distribución está sesgada hacia la derecha.


```{r Normalidad total.sulfur.dioxide}
isNormalAnderson('total.sulfur.dioxide')
create_plot('total.sulfur.dioxide')
```

Después de aplicar la transformación sigue sin tener una distribución normal.

```{r Normalidad total.sulfur.dioxide.transform}
create_plot('total.sulfur.dioxide') + scale_x_log10()
```

#### 4.2.1.8 density

Tiene una distribución normal.

```{r Normalidad density}
isNormalAnderson('density')
create_plot('density',df_wine_no_outliers,0.001)
```

#### 4.2.1.9 pH

Tiene una distribución normal.

```{r Normalidad pH}
isNormalAnderson('pH')
create_plot('pH')
```

#### 4.2.1.10 sulphates

Tiene una distribución sesgada hacia el lado derecho.
```{r Normalidad sulphates}
isNormalAnderson('sulphates')
create_plot('sulphates')
```

Al aplicar la transformada (log10) vemos que aparece una distribución normal.

```{r Normalidad sulphates.transform}
create_plot('sulphates')+scale_x_log10()
df_wine_trans$sulphates<-log10(df_wine_trans$sulphates)

```

#### 4.2.1.11 alcohol

```{r Normalidad alcohol}
isNormalAnderson('alcohol')
create_plot('alcohol')
```

Al aplicar la transformada (log10) vemos que aparece una distribución normal.


```{r Normalidad alcohol.transform}
create_plot('alcohol')+scale_x_log10()
df_wine_trans$alcohol<-log10(df_wine_trans$alcohol)

```

```{r Normalidad save.transform}
write.csv(df_wine_trans,"./df_wine_trans.csv", row.names = TRUE)
```

Seguidamente, pasamos a estudiar la homogeneidad de varianzas mediante la aplicación de un test de Fligner-Killeen. En este caso, estudiaremos esta homogeneidad en cuanto a los grupos conformados por los vehículos que presentan un motor turbo frente a un motor estándar. En el siguiente test, la hipótesis nula consiste en que ambas varianzas son iguales

### 4.2.2 Comprobación homogeneidad de la varianza

Pasamos a estudiar la homogeneidad de varianzas mediante la aplicación del test de Fligner-Killeen. En este caso, estudiaremos la homogeneidad de los grupos conformados por los vinos tintos y blancos. En el siguiente test, la hipótesis nula consiste en que ambas varianzas son iguales.

```{r Homogeneidad}
fligner.test(quality ~ wine.colour, data = df_wine_trans)
```
Puesto que obtenemos un p-valor superior a 0,05, aceptamos la hipótesis de que las varianzas de ambas muestras son homogéneas.


## 4.3. Aplicación de pruebas estadísticas para comparar los grupos de datos. En función de los datos y el objetivo del estudio, aplicar pruebas de contraste de hipótesis, correlaciones, regresiones, etc. Aplicar al menos tres métodos de análisis diferentes.

### 4.3.1 ¿Cómo impacta las variables cuantitativas por tipo de vino?

Vamos a crear una función que nos permita visualizar las variables a comparar, mediante un gráfico de boxplot y su distribución.
```{r createGroupComp}

create_plotComp <- function(var_name_A,var_name_B,ylim=2,xlim=0,data_val=df_wine_trans, val_alpha = 0.5) {
  var_name_A <- rlang::sym(var_name_A)
  var_name_B <- rlang::sym(var_name_B)
  return(grid.arrange(
  ggplot(data_val, aes(x = "", y = !! var_name_A, fill = !! var_name_B)) +
  geom_jitter(alpha = 0.5) + geom_boxplot(alpha = 0.5, color = "white"),
  ggplot(data_val, aes(x=!!var_name_A, fill = !! var_name_B)) +
  geom_histogram(position = "identity", alpha = 0.5) +
  coord_cartesian(xlim = c(xlim, ylim)), ncol = 2))
  }
```


#### 4.3.1.1 Volatile.acidity vs Wine.colour


```{r volatile.acidity.Vs.wine.colour}
create_plotComp('volatile.acidity','wine.colour',1.4)
```

```{r summario.volatile.acidity}
by(df_wine_trans$volatile.acidity, df_wine_trans$wine.colour, summary)
```
La variable **volatile.acidity** es mucho más baja:

- Vino Blanco media 0.27
- Vino Tinto media 0.52

#### 4.3.1.2 Residual.sugar vs Wine.colour

```{r vResidual.sugar.Vs.wine.colour}
create_plotComp('residual.sugar','wine.colour',20)
```

```{r summario.residual.sugar}
by(df_wine_trans$residual.sugar, df_wine_trans$wine.colour, summary)
```
La concentración de azúcar residual (RS) en las muestras de vino blanco es mayor a las encontradas en el vino tinto:
- Vino Blanco: Supera los 16 g/dm^3, para una media de 5.5 g/dm^3.
- Vino Tinto: Supera los 56 g/dm^3, para una media de 2.2 g/dm^3.

#### 4.3.1.3 Citric.acid vs Wine.colour

```{r Citric.acid.Vs.wine.colour}
create_plotComp('citric.acid','wine.colour',1)
```

```{r summario.citric.acid}
by(df_wine_trans$citric.acid, df_wine_trans$wine.colour, summary)
```
La concentración de acido cítrico en las muestras de vino blanco es mayor a las encontradas en el vino tinto:
- Vino Blanco: Supera los 0.99 g/dm^3, para una media de 0.36 g/dm^3.
- Vino Tinto: Supera los 0.78 g/dm^3, para una media de 0.25 g/dm^3.


#### 4.3.1.4 Chlorides vs Wine.colour

```{r Chlorides.Vs.wine.colour}
create_plotComp('chlorides','wine.colour',0.2)
```

```{r summario.chlorides}
by(df_wine_trans$chlorides, df_wine_trans$wine.colour, summary)
```
La concentración de cloruro (sal) en las muestras de vino blanco es menor a las encontradas en el vino tinto:
- Vino Blanco: Supera los 0.18 g/dm^3, para una media de 0.04 g/dm^3.
- Vino Tinto: Supera los 0.19 g/dm^3, para una media de 0.08 g/dm^3.

#### 4.3.1.5 Alcohol vs Wine.colour

```{r volatile.alcohol.Vs.wine.colour}
create_plotComp('alcohol','wine.colour',14,8.5)
```

```{r summario.alcohol}
by(df_wine_trans$alcohol, df_wine_trans$wine.colour, summary)
```
La concentración de alcohol para los diferentes tipos de vino son muy similares.

### 4.3.2 ¿Qué variables cuantitativas influyen más en la calidad?
A continuación, vamos a visualizar la matriz de correlación entre los diferentes atributos que componen el conjunto de datos para responder a la pregunta

```{r Correlacion}
col <- colorRampPalette(c(
  "#7F0000", "red", "#FF7F00", "#7FFF7F",
  "magenta", "#007FFF", "blue", "#00007F"
))

corrplot(
  cor(df_wine_trans[, 1:12]),
  method = "number",
  col = col(12),
  tl.col = "black",
  tl.srt = 45,
  type='upper',
  title = "Matriz de correlación",
  mar = c(0, 0, 1, 0),
  number.cex = 8 / ncol(df_wine_trans[, 1:12]),
  tl.cex = 1
)
```
#### Conclusiones
La matriz de correlación muestra una alta correlación entre la variable objetivo "quality" y "alcohol", siendo la más alta con un 0,44. Además, las siguientes variables muestran un alto nivel de correlación: sulphates, pH,free.sulfur.dioxide y citric.acid.
A tener en cuenta, que existe una correlación negativa entre la quality y el conjunto de variables: density, chlorides, residual.sugar y fixed.acidity.

### 4.3.3 ¿La calidad del vino tinto es mayor que la del vino blanco?

La prueba estadística consiste en un contraste de hipótesis sobre dos muestras, generadas a razón de la variables colour para determinar si la caldiad de los vinos depende del color (red o white). Generamos dos muestras: 

- 1) Los registros de vino Tinto, color = red. 
- 2) Los registros de vino Blanco, color = white. 

Se debe destacar que un test paramétrico como el que a continuación se utiliza necesita que los datos sean normales, si la muestra es de tamaño inferior a 30. Como en nuestro caso,
n > 30, el contraste de hipótesis siguiente es válido (aunque podría utilizarse un test no paramétrico como el de Mann-Whitney, que podría resultar ser más eficiente para este caso).

```{r Hipotesis}
df_wine_trans.red.quality <- df_wine_trans[df_wine_trans$wine.colour == "red",]$quality
df_wine_trans.white.quality <- df_wine_trans[df_wine_trans$wine.colour == "white",]$quality
```

Así, se plantea el siguiente contraste de hipótesis de dos muestras sobre la diferencia
de medias, el cual es unilateral atendiendo a la formulación de la hipótesis alternativa:
H0 : µ1 − µ2 = 0
H1 : µ1 − µ2 < 0
donde µ1 es la media de la población de la que se extrae la primera muestra y µ2 es la media
de la población de la que extrae la segunda. Así, tomaremos α = 0, 05.
```{r Hipotesis.Test}
t.test(df_wine_trans.red.quality, df_wine_trans.white.quality,alternative = "less")
```

Puesto que obtenemos un p-valor menor que el valor de significación fijado, rechazamos la
hipótesis nula. Por tanto, podemos concluir que, efectivamente, la calidad de un vino es
superior si es realizado para vino tinto.


### 4.3.4 Modelo regresión lineal

Para obtener un modelo de regresión lineal relativamente eficiente, crearemos varios modelos de regresión utilizando las variables que estén más correladas con
respecto a la variable objetivo "quality", según la matriz de correlación obtenida en el punto 4.3.2. Utilizaremos el criterio de mayor coeficiente de determinación (R2) para seleccionar el mejor modelo.

La matriz de correlación muestra una alta correlación entre la variable objetivo "quality" y "alcohol", siendo la más alta con un 0,44. Además, las siguientes variables muestran un alto nivel de correlación: sulphates, pH,free.sulfur.dioxide y citric.acid.
A tener en cuenta, que existe una correlación negativa entre la quality y el conjunto de variables: density, chlorides, residual.sugar y fixed.acidity.


```{r RegresionLineal}
# Regresores cuantitativos con mayor coeficiente o coeficiente inverso
alcohol = df_wine_trans$alcohol
sulphates = df_wine_trans$sulphates
pH = df_wine_trans$pH
free.sulfur.dioxide = df_wine_trans$free.sulfur.dioxide
citric.acid = df_wine_trans$citric.acid
density = df_wine_trans$density
chlorides = df_wine_trans$chlorides
residual.sugar = df_wine_trans$residual.sugar
fixed.acidity = df_wine_trans$fixed.acidity

# Regresores cualitativos
wine.colour = df_wine_trans$wine.colour

# Variable objetivo
quality = df_wine_trans$quality

# Generación de varios modelos
modelo1 <- lm(quality ~ alcohol + sulphates + pH + free.sulfur.dioxide +
citric.acid + density, data = df_wine_no_outliers)
modelo2 <- lm(quality ~ alcohol + fixed.acidity + residual.sugar + chlorides +
citric.acid + density, data = df_wine_no_outliers)
modelo3 <- lm(quality ~ sulphates + fixed.acidity + residual.sugar + chlorides +
citric.acid + density, data = df_wine_no_outliers)
modelo4 <- lm(quality ~ alcohol + sulphates + pH + free.sulfur.dioxide, data = df_wine_no_outliers)
modelo5 <- lm(quality ~ alcohol + wine.colour + sulphates + pH + free.sulfur.dioxide +
citric.acid + density, data = df_wine_no_outliers)
```

Para los anteriores modelos de regresión lineal múltiple vamos a utilizar el coeficiente de determinación para medir la bondad de los ajustes y quedarnos con aquel
modelo que mejor coeficiente presente.

```{r RegresionLineal.Coeficientes}

# Tabla con los coeficientes de determinación de cada modelo
tabla.coeficientes <- matrix(c(1, summary(modelo1)$r.squared,
                               2, summary(modelo2)$r.squared,
                               3, summary(modelo3)$r.squared,
                               4, summary(modelo4)$r.squared,
                               5, summary(modelo5)$r.squared),
ncol = 2, byrow = TRUE)
colnames(tabla.coeficientes) <- c("Modelo", "R^2")
tabla.coeficientes
```
El modelo número 1, ha sacado mejor coeficiente (R^2) utilizando ese modelo vamos a realizar una predicción de la calidad:

```{r RegresionLineal.Prediccion}
newdata <- data.frame(alcohol = 12.0,
                      sulphates = 0.55,
                      pH = 3.10,
                      free.sulfur.dioxide = 15,
                      citric.acid = 0.03,
                      density = 0.99968)
# Predecir la calidad
predict(modelo1, newdata)
```

# 5. Representación de los resultados a partir de tablas y gráficas.
Durante la realización del documento, están integrados los resultados en tablas y gráficas.


# 6. Resolución del problema. A partir de los resultados obtenidos, ¿cuáles son las conclusiones? ¿Los resultados permiten responder al problema?

A partir de las pruebas realizadas a lo largo de la práctica, hemos podido ir concretando las respuestas a las múltiples preguntas realizadas. 

- La eliminación de los outliers ha mejorado la normalización de la distribución de los datos para evitar trabajar con datos que nos pudieran hacer errar. Se ha disponibilizado el fichero resultante [df_wine_no_outliers.csv](https://gitlab.com/dbragut1/winecleanandanalisis/-/blob/main/data/df_wine_no_outliers.csv).
- El análisis univariante ha incrementado el conocimiento sobre cada una de las variables. Se ha disponibilizado el fichero resultante [df_wine_trans.csv](https://gitlab.com/dbragut1/winecleanandanalisis/-/blob/main/data/df_wine_trans.csv).
- El análisis bivariante ha incrementado el conocimiento sobre el comportamiento de cada una de las variables sobre la variable objetivo. Y nos ha ayudado a concretar las hipótesis adecuadas.
- Finalmente la generación de la hipótesis y la creación del modelo de regresión nos han permitido cubrir con las dudas planteadas.

En resumen, el análisis ha demostrado que la calidad del vino depende de las variables de entrada fisicoquímicas estando correlaciono positivamente con el alcohol, el dióxido de azufre libre, el sulfato y el pH. El principal desafío de este análisis de datos fue comprender el conjunto de datos y las variables para identificar la relación, patrón o correlación entre las variables y obtener una buena comprensión sobre el conjunto de datos.


# 7. Código: Hay que adjuntar el código, preferiblemente en R, con el que se ha realizado la limpieza, análisis y representación de los datos. Si lo preferís, también podéis trabajar en Python.
Para que fuera más comprensible el código está visible a lo largo del documento realizado en R Markdown.

# 8. Referencias

- Calvo M., Subirats L., Pérez D. (2019). Introducción a la limpieza y análisis de los datos. Editorial UOC.
- Megan Squire (2015). Clean Data. Packt Publishing Ltd.
- Jiawei Han, Micheine Kamber, Jian Pei (2012). Data mining: concepts and techniques.
Morgan Kaufmann.
- Jason W. Osborne (2010). Data Cleaning Basics: Best Practices in Dealing with Extreme Scores. Newborn and Infant Nursing Reviews; 10 (1): pp. 1527-3369.
- Peter Dalgaard (2008). Introductory statistics with R. Springer Science & Business
Media.
- Wes McKinney (2012). Python for Data Analysis. O’Reilley Media, Inc.
- Tutorial de Github https://guides.github.com/activities/hello-world.
- Guía para visualización con ggplot2 http://www.sthda.com/english/wiki/be-awesome-in-ggplot2-a-practical-guide-to-be-highly-effective-r-software-and-data-visualization
- Detalle sobre caracteristicas fisicoquímicas del sulphur dioxide (so2) http://www.winesofbalkans.com/sulphur-dioxide-so2-the-single-most-useful-additive-in-the-winemaking-process.html
- Guía para la generación de paletas e colores https://quantdev.ssri.psu.edu/tutorials/generating-custom-color-palette-function-r
- Guía para la personalización de gráficos https://rkabacoff.github.io/datavis/Customizing.html



